import 'package:flutter/material.dart';
import 'package:tomisha/constants/app_colors.dart';

/// All the text styles used in the app
class TextStyles {
  // Used when showing the task name in the main grid
  static const taskName = TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 17,
    letterSpacing: 0.0,
  );

  static const heading1Large = TextStyle(
    fontWeight: FontWeight.bold,
    color: AppColors.black,
    fontSize: 62,
  );

  static const heading1Small = TextStyle(
    fontWeight: FontWeight.w400,
    color: AppColors.black,
    fontSize: 42,
  );

  static const heading2Large = TextStyle(
    fontWeight: FontWeight.bold,
    color: AppColors.black,
    fontSize: 40,
  );

  static const heading2Small = TextStyle(
    color: AppColors.lightblack,
    fontSize: 21,
  );

  static const contentLarge = TextStyle(
      fontWeight: FontWeight.w400, fontSize: 30, color: AppColors.darkerGrey);

  static const contentSmall = TextStyle(
      fontWeight: FontWeight.w400, fontSize: 16, color: AppColors.lighterGrey);

  static const buttonActive = TextStyle(
      fontWeight: FontWeight.w700, fontSize: 17, color: AppColors.lightercyan);

  static const button = TextStyle(
      fontWeight: FontWeight.w700, fontSize: 17, color: AppColors.darkercyan);
}
