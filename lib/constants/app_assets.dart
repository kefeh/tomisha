class AppAssets {
  // system UI
  static const agreement = 'assets/undraw_agreement_aajr.svg';
  static const businessDeal = 'assets/undraw_business_deal_cpi9.svg';
  static const jobOffer = 'assets/undraw_job_offers_kw5d.svg';
  static const profileData = 'assets/undraw_Profile_data_re_v81r.svg';
  static const aboutMe = 'assets/undraw_about_me_wa29.svg';
  static const swipeProfiles = 'assets/undraw_swipe_profiles1_i6mr.svg';
  static const personalFile = 'assets/undraw_personal_file_222m.svg';
  static const task = 'assets/undraw_task_31wc.svg';
}
