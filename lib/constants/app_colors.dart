import 'package:flutter/material.dart';

class AppColors {
  static const white = Color(0xFFF6F6F6);
  static const lightestGrey = Color(0xFFDDDDDD);
  static const lighterGrey = Color(0xFFC1C1C1);
  static const grey = Color(0xFFCBD5E0);
  static const darkerGrey = Color(0xFF718096);
  static const lightText = Color(0xFFC4C4C4);
  static const black = Color(0xFF2D3748);
  static const lightblack = Color(0xFF4A5568);
  static const cyan = Color(0xFF81E6D9);
  static const lightercyan = Color(0xFFE6FFFA);
  static const darkercyan = Color(0xFF319795);
  static const blue = Color(0xFF3182CE);
  static const lighterblue = Color(0xFFEBF4FF);
  static const darkbrown = Color(0xFF707070);
  static const lightdarkbrown = Color.fromARGB(51, 0, 0, 0);
  static const lighterdarkbrown = Color.fromARGB(41, 0, 0, 0);
}
