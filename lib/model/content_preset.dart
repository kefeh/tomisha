import 'package:tomisha/constants/app_assets.dart';

class ContentPreset {
  const ContentPreset({required this.title, required this.image});
  final String title;
  final String image;

  @override
  String toString() => 'ContentPreset($title, $image)';

  static const List<ContentPreset> firstPage = [
    ContentPreset(
        title: 'Erstellen dein Lebenslauf', image: AppAssets.profileData),
    ContentPreset(title: 'Erstellen dein Lebenslauf', image: AppAssets.task),
    ContentPreset(
        title: 'Mit nur einem Klick bewerben', image: AppAssets.personalFile),
  ];
  static const List<ContentPreset> secondPage = [
    ContentPreset(
        title: 'Erstellen dein Urternehmensprofil',
        image: AppAssets.profileData),
    ContentPreset(title: 'Erstellen ein Jobinserat', image: AppAssets.aboutMe),
    ContentPreset(
        title: 'Wähle deinen neuen Mitarbeiter aus',
        image: AppAssets.swipeProfiles),
  ];
  static const List<ContentPreset> thirdPage = [
    ContentPreset(
        title: 'Erstellen dein Urternehmensprofil',
        image: AppAssets.profileData),
    ContentPreset(
        title: 'Erhalte Vermittlungs- angebot von Arbeitgeber',
        image: AppAssets.jobOffer),
    ContentPreset(
        title: 'Vermittlung nach Provision oder Stundenlohn',
        image: AppAssets.businessDeal),
  ];
}
