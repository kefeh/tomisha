import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tomisha/constants/app_assets.dart';
import 'package:tomisha/constants/app_colors.dart';
import 'package:tomisha/constants/text_styles.dart';
import 'package:tomisha/ui/painters/custom_painters.dart';
import 'package:tomisha/ui/widgets/main_button.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: AppColors.white,
      body: SizedBox(
        height: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                // color: AppColors.blue,
                width: double.infinity,
                height: height,
                child: Stack(
                  children: [
                    SizedBox(
                      width: double.infinity,
                      height: height,
                      child: CustomPaint(
                        painter: CurvePainter(),
                      ),
                    ),
                    SizedBox(
                      height: height / 2,
                      child: Padding(
                        padding: const EdgeInsets.all(60.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const SizedBox(
                              width: 320,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "Deine Job website",
                                    style: TextStyles.heading1Large,
                                  ),
                                  MainButton(size: 320)
                                ],
                              ),
                            ),
                            const SizedBox(width: 80),
                            AspectRatio(
                              aspectRatio: 1.0,
                              child: Container(
                                decoration: BoxDecoration(
                                  color: AppColors.white,
                                  borderRadius: BorderRadius.circular(455),
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(455),
                                  child: SvgPicture.asset(
                                    AppAssets.agreement,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                color: AppColors.cyan,
                height: height,
              )
            ],
          ),
        ),
      ),
    );
  }
}
