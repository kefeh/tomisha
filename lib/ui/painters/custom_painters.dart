import 'package:flutter/material.dart';
import 'package:tomisha/constants/app_colors.dart';

class CurvePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.style = PaintingStyle.fill;
    const Gradient gradient =
        LinearGradient(colors: [AppColors.lightercyan, AppColors.lighterblue]);

    var path = Path();

    path.moveTo(0, size.height * 0.5);
    path.quadraticBezierTo(size.width * 0.25, size.height * 0.5,
        size.width * 0.5, size.height * 0.45);
    path.quadraticBezierTo(
        size.width * 0.75, size.height * 0.4, size.width, size.height * 0.45);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);

    paint.shader = gradient.createShader(Offset.zero & size);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class CurvePainter2 extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.style = PaintingStyle.fill;
    const Gradient gradient =
        LinearGradient(colors: [AppColors.lightercyan, AppColors.lighterblue]);

    var path = Path();

    path.moveTo(0, size.height * 0.5);
    path.quadraticBezierTo(size.width * 0.25, size.height * 0.5,
        size.width * 0.5, size.height * 0.4);
    path.quadraticBezierTo(
        size.width * 0.75, size.height * 0.32, size.width, size.height * 0.3);
    path.lineTo(size.width, size.height * 0.15);
    path.quadraticBezierTo(size.width * 0.75, size.height * 0.1,
        size.width * 0.5, size.height * 0.1);
    path.quadraticBezierTo(
        size.width * 0.25, size.height * 0.15, 0, size.height * 0.12);
    path.lineTo(0, size.height * 0.5);
    // canvas.drawPath(path, paint);

    paint.shader = gradient.createShader(Offset.zero & size);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
