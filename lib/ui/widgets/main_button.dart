import 'package:flutter/material.dart';
import 'package:tomisha/constants/app_colors.dart';
import 'package:tomisha/constants/text_styles.dart';

class MainButton extends StatelessWidget {
  const MainButton({
    super.key,
    required this.size,
  });
  final double size;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        width: size,
        padding: const EdgeInsets.all(12),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          gradient: const LinearGradient(
            colors: [
              AppColors.darkercyan,
              AppColors.blue,
            ],
          ),
        ),
        child: const Text(
          "Kostenlos Registrieren",
          style: TextStyles.buttonActive,
        ),
      ),
    );
  }
}
